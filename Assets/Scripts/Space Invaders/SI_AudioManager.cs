﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SI_AudioManager : MonoBehaviour {

    private static SI_AudioManager instance;
    public static SI_AudioManager Instance
    {
        get
        {
            if (instance == null)
            {
                GameObject newGo = new GameObject();
                instance = newGo.AddComponent<SI_AudioManager>();
            }
            return instance;
        }
    }

    private List<AudioSource> sources = new List<AudioSource>();

    public void Play(AudioClip clip, float volume = 0.5f)
    {
        AudioSource source = GetAudioSource();
        source.clip = clip;
        source.volume = volume;
        source.Play();
    }

    private AudioSource GetAudioSource()
    {
        foreach(AudioSource source in sources)
        {
            if (!source.isPlaying)
            {
                return source;
            }
        }
        GameObject go = new GameObject();
        AudioSource newSource = go.AddComponent<AudioSource>();
        sources.Add(newSource);
        return newSource;
    }
}
