﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SI_BaseMovement : MonoBehaviour {

    [SerializeField]
    protected float startRadius;
    [SerializeField]
    protected Transform origin;
    [SerializeField]
    protected float moveSpeed;

    protected float currentAngle;

}
