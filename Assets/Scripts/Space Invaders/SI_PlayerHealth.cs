﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SI_PlayerHealth : MonoBehaviour, IProjectileInteraction {

    [SerializeField]
    private RectTransform healthDisplayRect;
    [SerializeField]
    private AudioClip playerHitClip;

    private float health = 100;

    public void Interact(SI_BaseProjectile projectile)
    {
        if (projectile.tag != "Player")
        {
            health -= 10;
            if (health < 0)
            {
                health = 0;
            }
            healthDisplayRect.localScale = new Vector2(health / 100.0f, healthDisplayRect.localScale.y);
            GameObject.Destroy(projectile.gameObject);
            if (health <= 0)
            {
                SI_GameManager.Instance.PlayerDeath();
            }
            if(playerHitClip)
            SI_AudioManager.Instance.Play(playerHitClip,0.8f);
        }

    }
}
