﻿using UnityEngine;

public abstract class SI_BaseGun : MonoBehaviour {

    [SerializeField]
    private GameObject projectilePrefab;
    [SerializeField]
    private Transform firingPosition;
    [SerializeField]
    private float firingSpeed;
    [SerializeField]
    private float firingCooldown;

    private float cooldown;

	public void Fire()
    {
        if(cooldown <= 0)
        {
            GameObject newProjectile = GameObject.Instantiate(projectilePrefab, firingPosition.transform.position, firingPosition.transform.rotation);
            SI_BaseProjectile projectile = newProjectile.GetComponentInChildren<SI_BaseProjectile>();
            AddProjectileModifiers(projectile);
            projectile.Fire(firingSpeed);
            cooldown = firingCooldown;
            AudioSource source = GetComponent<AudioSource>();
            if (source)
            {
                source.Play();
            }
        }
    }

    protected abstract void AddProjectileModifiers(SI_BaseProjectile projectile);

    protected virtual void Update()
    {
        if (cooldown > 0)
        {
            cooldown -= Time.deltaTime;
        }
    }
}
