﻿using UnityEngine;

public class RandomMove : MonoBehaviour {

    [SerializeField]
    float checkForSwitchTime = 2.0f;
    [SerializeField]
    float speed = 1;
    [SerializeField]
    float magnitude = 0.2f;

    float xTime;
    float yTime;
    int xDir;
    int yDir;
    Vector3 oldOffset = Vector3.zero;
    float switchTimer;

	// Use this for initialization
	void Start () {
        RandomizeDirections();
        RandomizeTime();
        SetPosition();
	}

    private void SetPosition()
    {
        Vector3 originalPos = transform.position - oldOffset;
        oldOffset = new Vector3(Mathf.Sin(xTime) * magnitude, Mathf.Cos(yTime) * magnitude);
        transform.position += oldOffset;
    }

    private void RandomizeTime()
    {
        xTime = Random.Range(0, 6.8f);
        yTime = Random.Range(0, 6.8f);
    }

    private void RandomizeDirections()
    {
        xDir = Random.value > 0.5f ? -1 : 1;
        yDir = Random.value > 0.5f ? -1 : 1;
        switchTimer = checkForSwitchTime;
    }

    // Update is called once per frame
    void Update () {
        xTime += Time.deltaTime * speed *xDir;
        yTime += Time.deltaTime * speed * yDir;
        SetPosition();
        if (switchTimer > 0)
        {
            switchTimer -= Time.deltaTime;
        }
        else
        {
            RandomizeDirections();
        }
	}
}
