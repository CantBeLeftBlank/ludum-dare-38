﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Yarn.Unity;
using Yarn.Unity.Example;

[RequireComponent(typeof(Animator)), RequireComponent(typeof(NavMeshAgent))]
public class PlayerMovement : MonoBehaviour {


    private Animator animator;
    private NavMeshAgent agent;

    [SerializeField]
    private float moveSpeed = 2f;
    [SerializeField]
    private float turnSmoothing = 15f;
    [SerializeField]
    private float interactionRadius = 2.0f;

    private void Start()
    {
        animator = this.GetComponent<Animator>();
        agent = this.GetComponent<NavMeshAgent>();
    }

    void Update () {
        if (FindObjectOfType<DialogueRunner>().isDialogueRunning == false)
        {
            ProcessInput();
        }
        Animate();
	}

    private void Animate()
    {
        animator.SetFloat(Animator.StringToHash("Speed"), agent.velocity.magnitude);
    }

    private void ProcessInput()
    {
        Vector3 forwardVector = Camera.main.gameObject.transform.forward;
        Vector3 rightVector = Camera.main.gameObject.transform.right;
        forwardVector.y = rightVector.y= 0;
        forwardVector.Normalize();
        rightVector.Normalize();
        rightVector *= Input.GetAxis("Horizontal") * moveSpeed;
        forwardVector *= Input.GetAxis("Vertical") * moveSpeed;

        agent.velocity = (forwardVector + rightVector);

        if (agent.velocity.magnitude > 0)
        {
            Quaternion targetRotation = Quaternion.LookRotation(agent.velocity);
            transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, turnSmoothing * Time.deltaTime);
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            CheckForNearbyNPC();
        }
    }

    public void CheckForNearbyNPC()
    {
        // Find all DialogueParticipants, and filter them to
        // those that have a Yarn start node and are in range; 
        // then start a conversation with the first one
        var allParticipants = new List<NPC>(FindObjectsOfType<NPC>());
        var target = allParticipants.Find(delegate (NPC p) {
            return string.IsNullOrEmpty(p.talkToNode) == false && // has a conversation node?
            (p.transform.position - this.transform.position)// is in range?
            .magnitude <= interactionRadius;
        });
        if (target != null)
        {
            // Kick off the dialogue at this node.
            FindObjectOfType<DialogueRunner>().StartDialogue(target.talkToNode);
        }
    }
}
