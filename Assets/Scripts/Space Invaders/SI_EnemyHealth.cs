﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SI_EnemyHealth : MonoBehaviour, IProjectileInteraction
{

    [SerializeField]
    public AudioClip deathSound;
    [SerializeField]
    private AudioClip hitSound;
    [SerializeField]
    private GameObject deathPrefab;

    private float health = 100;

    public void Interact(SI_BaseProjectile projectile)
    {
        if (projectile.tag == "Player")
        {
            if (hitSound)
                SI_AudioManager.Instance.Play(hitSound);

            health -= 50;
            if (health <= 0)
            {
                SI_GameManager.Instance.AddToScore(100);
                if (deathPrefab)
                    GameObject.Instantiate(deathPrefab, transform.position, transform.rotation);
                if (deathSound)
                    SI_AudioManager.Instance.Play(deathSound, 0.2f);
                Destroy(gameObject);
            }
            Destroy(projectile.gameObject);
            
        }
    }

}
