﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class PlayerCamera : MonoBehaviour {

    [SerializeField]
    private Transform playerTransform;
    [SerializeField]
    private float trackingSpeed = 15.0f;

    private float keepAtDistance;

	// Use this for initialization
	void Start () {
        if (playerTransform != null)
        {
            keepAtDistance = Vector3.Distance(transform.position, playerTransform.position);
        }
        else
        {
            Debug.LogError("[PlayerCamera] player transform not set");
            this.enabled = false;
        }
	}
	
	// Update is called once per frame
	void Update () {
        transform.position = Vector3.Lerp(transform.position, playerTransform.position - transform.forward * keepAtDistance, trackingSpeed * Time.deltaTime);
    }
}
