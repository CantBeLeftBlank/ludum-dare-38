﻿
using System;

public static class StringExtensions
{

    public static string GetUntilOrEmpty(this string text, string stopAt = "-")
    {
        int charLocation = text.IndexOf(stopAt, StringComparison.Ordinal);

        if (charLocation > 0)
        {
            return text.Substring(0, charLocation);
        }

        return String.Empty;
    }
}
