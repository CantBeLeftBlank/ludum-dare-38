﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConstantRotation : MonoBehaviour {

    [SerializeField]
    private Vector3 rotationVector;
	
	// Update is called once per frame
	void Update () {
        transform.localEulerAngles += rotationVector * Time.deltaTime;
	}
}
