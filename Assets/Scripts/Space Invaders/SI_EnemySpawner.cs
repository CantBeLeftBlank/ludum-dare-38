﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SI_EnemySpawner : MonoBehaviour {


    [SerializeField]
    private EnemyWave[] waves;
    
    private EnemyWave currentWave;
    private int waveIndex = 0;

    [System.Serializable]
    private class EnemyWave
    {
        public SpawnInfo[] Spawns;

        internal bool SpawnWave(Transform origin)
        {
            bool finished = true;
            foreach(SpawnInfo spawnInfo in Spawns)
            {
                if (spawnInfo.SpawnsLeft > 0)
                {
                    spawnInfo.SpawnTimer += Time.deltaTime;

                    if(spawnInfo.SpawnTimer > spawnInfo.SpawnDelay)
                    {
                        spawnInfo.SpawnTimer = 0;
                        //TODO: Set position
                        GameObject go = GameObject.Instantiate(spawnInfo.SpawnPrefab);
                        SI_EnemyMovement movement = go.GetComponentInChildren<SI_EnemyMovement>();
                        movement.SetStartPosition(9.0f, UnityEngine.Random.Range(0, 6.8f));
                        movement.SetOrigin(origin);
                        spawnInfo.SpawnsLeft--;
                    }
                    finished = false;
                }
            }
            return finished;
        }
    }
	[System.Serializable]
    private class SpawnInfo
    {
        public GameObject SpawnPrefab;
        public int SpawnsLeft;
        public float SpawnDelay = 1.0f;
        public float SpawnTimer;
    }
	// Update is called once per frame
	void Update () {
        if (currentWave == null)
        {
            if(waveIndex < waves.Length)
            currentWave = waves[waveIndex];
        }
        else
        {
            if (currentWave.SpawnWave(transform))
            {
                waveIndex++;
                currentWave = null;
            }
        }
	}
}
