﻿Shader "Custom/CustomLightingGI" {
	 Properties {
            _MainTex ("Albedo (RGB)", 2D) = "white" {}
			_Color("Main Color",Color) = (1,1,1,1)
			_BlingPhong ("BlingPhong", Range(0,1)) = 0.5
        }
        SubShader {
            Tags { "RenderType"="Opaque" }
            
            CGPROGRAM
            #pragma surface surf StandardDefaultGI fullforwardshadows
    
            #include "UnityPBSLighting.cginc"
    
            sampler2D _MainTex;
			fixed4 _Color;
			half _BlingPhong;
    
            inline half4 LightingStandardDefaultGI(SurfaceOutputStandard s, half3 viewDir, UnityGI gi)
            {
                 s.Normal = normalize(s.Normal);

				half oneMinusReflectivity;
				half3 specColor;
				s.Albedo = DiffuseAndSpecularFromMetallic (s.Albedo, s.Metallic, /*out*/ specColor, /*out*/ oneMinusReflectivity);

				// shader relies on pre-multiply alpha-blend (_SrcBlend = One, _DstBlend = OneMinusSrcAlpha)
				// this is necessary to handle transparency in physically correct way - only diffuse component gets affected by alpha
				half outputAlpha;
				s.Albedo = PreMultiplyAlpha (s.Albedo, s.Alpha, oneMinusReflectivity, /*out*/ outputAlpha);

				half4 c1 = UNITY_BRDF_PBS (s.Albedo, specColor, oneMinusReflectivity, s.Smoothness, s.Normal, viewDir, gi.light, gi.indirect);
				 half NdotL = dot (s.Normal, gi.light.dir);
			  NdotL *= _BlingPhong;

			  half model = (1 * (1-_BlingPhong)) + (NdotL * _BlingPhong);

              half4 c;
              c.rgb = s.Albedo * _LightColor0.rgb * ((model * (1-_BlingPhong)) + _BlingPhong * c1);
              c.a = s.Alpha;
				c.rgb += UNITY_BRDF_GI (s.Albedo, specColor, oneMinusReflectivity, s.Smoothness, s.Normal ,viewDir, s.Occlusion, gi);
				c.a = outputAlpha;
				return c;
            }
    
            inline void LightingStandardDefaultGI_GI(
                SurfaceOutputStandard s,
                UnityGIInput data,
                inout UnityGI gi)
            {
                 gi = UnityGlobalIllumination (data, s.Occlusion, s.Smoothness, s.Normal);
            }
    
            struct Input {
                float2 uv_MainTex;
            };
    
            inline half4 CustomLambert(SurfaceOutputStandard s, half3 viewDir, UnityGI gi)
            {
              half NdotL = dot (s.Normal, gi.light.dir);
			  NdotL *= _BlingPhong;

			  half model = (1 * (1-_BlingPhong)) + (NdotL * _BlingPhong);

              half4 c;
              c.rgb = s.Albedo * _LightColor0.rgb * (model);
              c.a = s.Alpha;
              return c;
            }

    
    
            void surf (Input IN, inout SurfaceOutputStandard o) {
                o.Albedo = tex2D(_MainTex, IN.uv_MainTex) * _Color;
            }
            ENDCG
        }
        FallBack "Diffuse"
}
