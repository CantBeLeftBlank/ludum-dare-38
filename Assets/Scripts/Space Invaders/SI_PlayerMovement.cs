﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SI_PlayerMovement : SI_BaseMovement{

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        float speed = Input.GetAxis("Horizontal") * moveSpeed;

        currentAngle += speed;

        transform.position = origin.position + new Vector3(Mathf.Sin(currentAngle), Mathf.Cos(currentAngle),0 ) * startRadius;
        transform.LookAt(origin);
    }
}
