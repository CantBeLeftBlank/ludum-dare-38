﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SI_GameManager : MonoBehaviour {

	// Use this for initialization
	void Start () {
		if(instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
            if (scoreCanvas)
            {
                DontDestroyOnLoad(scoreCanvas);
            }
        }
        else
        {
            Destroy(gameObject);
        }
	}
    private static SI_GameManager instance;
    public static SI_GameManager Instance
    {
        get
        {
            if (instance == null)
            {
                GameObject newGo = new GameObject();
                instance = newGo.AddComponent<SI_GameManager>();
            }
            return instance;
        }
    }

    private int Score;
    [SerializeField]
    private GameObject scoreCanvas;
    [SerializeField]
    private Text scoreText;

    public void PlayerDeath()
    {
        if (scoreCanvas)
        {
            scoreCanvas.SetActive(true);
            scoreText.text = "Score: " + Score.ToString();
        }
        else
        {
            ReloadLevel();
        }
    }

    void OnEnable()
    {
        SceneManager.sceneLoaded += OnLevelFinishedLoading;
    }

    private void OnLevelFinishedLoading(Scene arg0, LoadSceneMode arg1)
    {
        Score = 0;
        if (scoreCanvas)
        {
            scoreCanvas.SetActive(false);
        }
    }

    void OnDisable()
    {
        SceneManager.sceneLoaded -= OnLevelFinishedLoading;
    }

    public void AddToScore(int amount)
    {
        Score += amount;
    }
    public void ReloadLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
