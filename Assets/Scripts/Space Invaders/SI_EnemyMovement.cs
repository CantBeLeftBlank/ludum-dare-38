﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SI_EnemyMovement : SI_BaseMovement
{

    private static List<SI_EnemyMovement> instances = new List<SI_EnemyMovement>();
    private static MovementClock moveClock;
    private float angleDisplacement;

    private void Start()
    {
        if (moveClock == null)
        {
            GameObject moveTimer = new GameObject("Util { Enemy Move Timer }");
            moveClock = moveTimer.AddComponent<MovementClock>();
        }
        SetPosition();
    }

    private void OnEnable()
    {
        instances.Add(this);
    }
    private void OnDisable()
    {
        instances.Remove(this);
    }

    private void Move()
    {
        if (startRadius > 6 || angleDisplacement > 3)
        {
            MoveTowardsOrigin();
        }
        else
        {
            RotateAroundOrigin();
        }
    }

    private void RotateAroundOrigin()
    {
        currentAngle += moveSpeed;
        SetPosition();
        angleDisplacement += moveSpeed;
        
    }

    internal void SetStartPosition(float radius, float angle)
    {
        startRadius = radius;
        currentAngle = angle;
    }

    internal void SetOrigin(Transform transform)
    {
        origin = transform;
    }

    private void SetPosition()
    {
        transform.position = origin.position + new Vector3(Mathf.Sin(currentAngle), Mathf.Cos(currentAngle), 0) * startRadius;
        transform.LookAt(origin);
    }

    private void MoveTowardsOrigin()
    {
        startRadius -= moveSpeed*5;
        SetPosition();
        angleDisplacement = 0;
    }

    private class MovementClock : MonoBehaviour
    {

        float time;

        void Update()
        {
            this.time += Time.deltaTime;
            if (this.time > 1.0f)
            {
                foreach (SI_EnemyMovement movementInstance in instances)
                {
                    movementInstance.Move();
                }
                this.time = 0;
            }
        }
    }


}
