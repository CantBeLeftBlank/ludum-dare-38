﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SI_BaseProjectile : MonoBehaviour {


    public abstract void Fire(float speed);

    protected virtual void OnTriggerEnter(Collider other)
    {
        IProjectileInteraction interaction = other.GetComponentInChildren<IProjectileInteraction>();
        if (interaction !=null)
        {
            interaction.Interact(this);
        }
    }
    
}
