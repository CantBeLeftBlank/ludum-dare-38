﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SI_Shield : MonoBehaviour, IProjectileInteraction {
    public void Interact(SI_BaseProjectile projectile)
    {
        if (projectile.tag != "Player")
        {
            GameObject.Destroy(projectile.gameObject);
            GameObject.Destroy(gameObject);
        }
    }
    
}
