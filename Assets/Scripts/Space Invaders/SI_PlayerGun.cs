﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SI_PlayerGun : SI_BaseGun {

    protected override void AddProjectileModifiers(SI_BaseProjectile projectile)
    {
        projectile.tag = "Player";
    }

    // Update is called once per frame
    protected override void Update () {
        base.Update();
        if (Input.GetKey(KeyCode.Space))
        {
            Fire();
        }
	}
}
