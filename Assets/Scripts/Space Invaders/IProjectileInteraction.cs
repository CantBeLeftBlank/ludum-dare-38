﻿public interface IProjectileInteraction {

    void Interact(SI_BaseProjectile projectile);
}
