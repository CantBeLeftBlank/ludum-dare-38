﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SI_LinearProjectile : SI_BaseProjectile{

    private Vector3 movementVector;

    public override void Fire(float speed)
    {
        movementVector = transform.forward * speed;
    }
	
	// Update is called once per frame
	void Update () {
        transform.position += movementVector * Time.deltaTime;
	}
}
