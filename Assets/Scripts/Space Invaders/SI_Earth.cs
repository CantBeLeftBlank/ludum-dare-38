﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SI_Earth : MonoBehaviour, IProjectileInteraction {

    [SerializeField]
    private Text populationDeathDisplay;
    [SerializeField]
    private Material happyFaceMat;
    [SerializeField]
    private Material sadFaceMat;
    [SerializeField]
    private MeshRenderer faceRenderer;

    private int currentDeaths;

    public void Interact(SI_BaseProjectile projectile)
    {
        if (projectile.tag != "Player")
        {
            currentDeaths += UnityEngine.Random.Range(100000, 500000);
            populationDeathDisplay.text = "Population Death: "+currentDeaths.ToString();
            CancelInvoke();
            faceRenderer.material = sadFaceMat;
            Invoke("SwitchToHappyFace", 0.5f);
            GameObject.Destroy(projectile.gameObject);

        }

    }
    void SwitchToHappyFace()
    {
        faceRenderer.material = happyFaceMat;
    }
    
}
